# Shared markdown editor
---
* This is a project created using Meteor and React.
* The purpose of this project is solely educational, it was only created to explore the Meteor and React frameworks so it should not be used for any serious things.

### Expected functionality

* We should be able to log in/out
* We should be able to create a bin
* We should be able to acces shared and our bins
* We should be able to edit markdown and see it parsed in realtime
----
----
### Steps for instalation

1. Install node and npm 
2. Install meteor
3. git clone git@bitbucket.org:ssuciu/shared-markdown-editor.git
4. cd shared-markdown-editor
5. meteor (type meteor in console and press enter) -- your app will be served on localhost:3000
