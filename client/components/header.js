import React, { Component } from 'react'
import Accounts from './accounts'
import { Link } from 'react-router-dom'

class Header extends Component {
  handleBinClick(event) {
    event.preventDefault()
    Meteor.call('bins.insert', (error, data) => {
      this.props.history.push(`/bins/${data}`)
    })
  }

  render() {
    return (
      <nav className="nav navbar-default">
        <div className="navbar-header">
          <Link to="/">
            <span className="navbar-brand">Shared Markdown Editor</span>
          </Link>
        </div>
        <ul className="nav navbar-nav">
          <li>
            <a>
              <Accounts />
            </a>
          </li>
          <li>
            <a href="#" onClick={this.handleBinClick.bind(this)}>
              Create Bin
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Header;
