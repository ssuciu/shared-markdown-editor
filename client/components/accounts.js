import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Template } from 'meteor/templating'
import {Blaze} from 'meteor/blaze'


class Accounts extends Component {
  componentDidMount() {
    //render the login form and stick in in the div ;)
    this.view = Blaze.render(Template.loginButtons, ReactDOM.findDOMNode(this.refs.container))
  }

  componentWillUnmount() {
   //we need to destroy the created forms (by ourselves)

  }

  render() {
    return (
      <div ref='container'></div>
    );
  }
}

export default Accounts;
