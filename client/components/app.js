import React, { Component } from 'react'
import Header from './header'
import BinsList from './bins/bins-list'
import BinsMain from './bins/bins-main'
import { BrowserRouter as Router, Link, Route } from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <Router>
      <div>
        <Route path="*" render={(props) => (
          <Header {...props}/>
        )} />
        <Route exact path="/" component={BinsList}/>
        <Route path="/bins/:binId" render={(props)=> (
          <BinsMain {...props}/>
        )}/>
      </div>
    </Router>
    );
  }
}

export default App;
