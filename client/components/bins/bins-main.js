import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'
import { Bins } from '../../../imports/collections/bins'
import BinsEditor from './bins-editor'
import BinsViewer from './bins-viewer'
import BinsShare from './bins-share'
import MDSpinner from "react-md-spinner";

class BinsMain extends Component {
  render() {
    if(!this.props.bin){return <div className='loading-spinner'><MDSpinner size={300}/></div>}
    return (
      <div>
        <BinsEditor bin={this.props.bin}/>
        <BinsViewer bin={this.props.bin}/>
        <BinsShare bin={this.props.bin}/>
      </div>
    )
  }
}

export default createContainer((props)=>{
  const id = props.match.params.binId
  Meteor.subscribe('bins');
  Meteor.subscribe('sharedBins')
  return {bin: Bins.findOne(id)}
}, BinsMain)
