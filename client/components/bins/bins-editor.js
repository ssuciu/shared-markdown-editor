import React, { Component } from 'react'
import CodeMirror from 'react-codemirror'
import 'codemirror/mode/markdown/markdown'

class BinsEditor extends Component {
  saveChanges(content) {
    Meteor.call('bin.update', this.props.bin, content)
  }
  render() {
    return (
      <div className="col-xs-7">
        <h3>
          <i className="material-icons">mode_edit</i>
          Input
        </h3>
        <CodeMirror
          value={this.props.bin.content}
          defaultValue=""
          onChange={this.saveChanges.bind(this)}
          options={{
            mode: 'markdown',
            lineNumbers: 'true'
          }} />
      </div>
    );
  }
}

export default BinsEditor
